$(function(){
	//Preguntamos si existe en la base de datos
	var rel = [];
	var count = 0;
	var country = [{"AF":"Afghanistan"},{"AX":"Åland Islands"},{"AL":"Albania"},{"DZ":"Algeria"},{"AS":"American Samoa"},{"AD":"AndorrA"},{"AO":"Angola"},{"AI":"Anguilla"},{"AQ":"Antarctica"},{"AG":"Antigua and Barbuda"},{"AR":"Argentina"},{"AM":"Armenia"},{"AW":"Aruba"},{"AU":"Australia"},{"AT":"Austria"},{"AZ":"Azerbaijan"},{"BS":"Bahamas"},{"BH":"Bahrain"},{"BD":"Bangladesh"},{"BB":"Barbados"},{"BY":"Belarus"},{"BE":"Belgium"},{"BZ":"Belize"},{"BJ":"Benin"},{"BM":"Bermuda"},{"BT":"Bhutan"},{"BO":"Bolivia"},{"BA":"Bosnia and Herzegovina"},{"BW":"Botswana"},{"BV":"Bouvet Island"},{"BR":"Brazil"},{"IO":"British Indian Ocean Territory"},{"BN":"Brunei Darussalam"},{"BG":"Bulgaria"},{"BF":"Burkina Faso"},{"BI":"Burundi"},{"KH":"Cambodia"},{"CM":"Cameroon"},{"CA":"Canada"},{"CV":"Cape Verde"},{"KY":"Cayman Islands"},{"CF":"Central African Republic"},{"TD":"Chad"},{"CL":"Chile"},{"CN":"China"},{"CX":"Christmas Island"},{"CC":"Cocos (Keeling) Islands"},{"CO":"Colombia"},{"KM":"Comoros"},{"CG":"Congo"},{"CD":"Congo, The Democratic Republic of the"},{"CK":"Cook Islands"},{"CR":"Costa Rica"},{"CI":"Cote D'Ivoire"},{"HR":"Croatia"},{"CU":"Cuba"},{"CY":"Cyprus"},{"CZ":"Czech Republic"},{"DK":"Denmark"},{"DJ":"Djibouti"},{"DM":"Dominica"},{"DO":"Dominican Republic"},{"EC":"Ecuador"},{"EG":"Egypt"},{"SV":"El Salvador"},{"GQ":"Equatorial Guinea"},{"ER":"Eritrea"},{"EE":"Estonia"},{"ET":"Ethiopia"},{"FK":"Falkland Islands (Malvinas)"},{"FO":"Faroe Islands"},{"FJ":"Fiji"},{"FI":"Finland"},{"FR":"France"},{"GF":"French Guiana"},{"PF":"French Polynesia"},{"TF":"French Southern Territories"},{"GA":"Gabon"},{"GM":"Gambia"},{"GE":"Georgia"},{"DE":"Germany"},{"GH":"Ghana"},{"GI":"Gibraltar"},{"GR":"Greece"},{"GL":"Greenland"},{"GD":"Grenada"},{"GP":"Guadeloupe"},{"GU":"Guam"},{"GT":"Guatemala"},{"GG":"Guernsey"},{"GN":"Guinea"},{"GW":"Guinea-Bissau"},{"GY":"Guyana"},{"HT":"Haiti"},{"HM":"Heard Island and Mcdonald Islands"},{"VA":"Holy See (Vatican City State)"},{"HN":"Honduras"},{"HK":"Hong Kong"},{"HU":"Hungary"},{"IS":"Iceland"},{"IN":"India"},{"ID":"Indonesia"},{"IR":"Iran, Islamic Republic Of"},{"IQ":"Iraq"},{"IE":"Ireland"},{"IM":"Isle of Man"},{"IL":"Israel"},{"IT":"Italy"},{"JM":"Jamaica"},{"JP":"Japan"},{"JE":"Jersey"},{"JO":"Jordan"},{"KZ":"Kazakhstan"},{"KE":"Kenya"},{"KI":"Kiribati"},{"KP":"Korea, Democratic People'S Republic of"},{"KR":"Korea, Republic of"},{"KW":"Kuwait"},{"KG":"Kyrgyzstan"},{"LA":"Lao People'S Democratic Republic"},{"LV":"Latvia"},{"LB":"Lebanon"},{"LS":"Lesotho"},{"LR":"Liberia"},{"LY":"Libyan Arab Jamahiriya"},{"LI":"Liechtenstein"},{"LT":"Lithuania"},{"LU":"Luxembourg"},{"MO":"Macao"},{"MK":"Macedonia, The Former Yugoslav Republic of"},{"MG":"Madagascar"},{"MW":"Malawi"},{"MY":"Malaysia"},{"MV":"Maldives"},{"ML":"Mali"},{"MT":"Malta"},{"MH":"Marshall Islands"},{"MQ":"Martinique"},{"MR":"Mauritania"},{"MU":"Mauritius"},{"YT":"Mayotte"},{"MX":"Mexico"},{"FM":"Micronesia, Federated States of"},{"MD":"Moldova, Republic of"},{"MC":"Monaco"},{"MN":"Mongolia"},{"MS":"Montserrat"},{"MA":"Morocco"},{"MZ":"Mozambique"},{"MM":"Myanmar"},{"NA":"Namibia"},{"NR":"Nauru"},{"NP":"Nepal"},{"NL":"Netherlands"},{"AN":"Netherlands Antilles"},{"NC":"New Caledonia"},{"NZ":"New Zealand"},{"NI":"Nicaragua"},{"NE":"Niger"},{"NG":"Nigeria"},{"NU":"Niue"},{"NF":"Norfolk Island"},{"MP":"Northern Mariana Islands"},{"NO":"Norway"},{"OM":"Oman"},{"PK":"Pakistan"},{"PW":"Palau"},{"PS":"Palestinian Territory, Occupied"},{"PA":"Panama"},{"PG":"Papua New Guinea"},{"PY":"Paraguay"},{"PE":"Peru"},{"PH":"Philippines"},{"PN":"Pitcairn"},{"PL":"Poland"},{"PT":"Portugal"},{"PR":"Puerto Rico"},{"QA":"Qatar"},{"RE":"Reunion"},{"RO":"Romania"},{"RU":"Russian Federation"},{"RW":"RWANDA"},{"SH":"Saint Helena"},{"KN":"Saint Kitts and Nevis"},{"LC":"Saint Lucia"},{"PM":"Saint Pierre and Miquelon"},{"VC":"Saint Vincent and the Grenadines"},{"WS":"Samoa"},{"SM":"San Marino"},{"ST":"Sao Tome and Principe"},{"SA":"Saudi Arabia"},{"SN":"Senegal"},{"CS":"Serbia and Montenegro"},{"SC":"Seychelles"},{"SL":"Sierra Leone"},{"SG":"Singapore"},{"SK":"Slovakia"},{"SI":"Slovenia"},{"SB":"Solomon Islands"},{"SO":"Somalia"},{"ZA":"South Africa"},{"GS":"South Georgia and the South Sandwich Islands"},{"ES":"Spain"},{"LK":"Sri Lanka"},{"SD":"Sudan"},{"SR":"Suriname"},{"SJ":"Svalbard and Jan Mayen"},{"SZ":"Swaziland"},{"SE":"Sweden"},{"CH":"Switzerland"},{"SY":"Syrian Arab Republic"},{"TW":"Taiwan, Province of China"},{"TJ":"Tajikistan"},{"TZ":"Tanzania, United Republic of"},{"TH":"Thailand"},{"TL":"Timor-Leste"},{"TG":"Togo"},{"TK":"Tokelau"},{"TO":"Tonga"},{"TT":"Trinidad and Tobago"},{"TN":"Tunisia"},{"TR":"Turkey"},{"TM":"Turkmenistan"},{"TC":"Turks and Caicos Islands"},{"TV":"Tuvalu"},{"UG":"Uganda"},{"UA":"Ukraine"},{"AE":"United Arab Emirates"},{"GB":"United Kingdom"},{"US":"United States"},{"UM":"United States Minor Outlying Islands"},{"UY":"Uruguay"},{"UZ":"Uzbekistan"},{"VU":"Vanuatu"},{"VE":"Venezuela"},{"VN":"Viet Nam"},{"VG":"Virgin Islands, British"},{"VI":"Virgin Islands, U.S."},{"WF":"Wallis and Futuna"},{"EH":"Western Sahara"},{"YE":"Yemen"},{"ZM":"Zambia"},{"ZW":"Zimbabwe"}];
	var page = 0;
	
	Query(query);

	function Query(query, st = true){
		$.post(api, {q:query, action:"search"}, function(data){
			if(st){
				if ( data == null){
					web(query);
				}
				else{
					set("data", JSON.stringify(data));
					rel = data.main_page.related;
					construct_time(data.time);
					if(data.main_page.mean != undefined) construct_didyoumean(query, data.main_page.mean);
					construct_webresult(data.main_page.web);
					addRelatedLikeQuery(rel);
					if (data["page_1"] != undefined) {
						construct_paginator(query);
					}
					if (data.widgets != undefined) construct_widget(data.widgets.widgets);
					if (data.video   != undefined){
						$("#apes a").eq(2).show();
						construct_videos(data.video.video);
					}
					if (data.images  != undefined){
						$("#apes a").eq(1).show();
						construct_images(data.images.image);
					}
					if(data.main_page.related != undefined) construct_related(rel);
					
					$("footer").show();
				}
				otherimages(query);
			} else {
				if ( data == null){
					web(query, false);
				}
			}
		}, "json");
	}
	function addRelatedLikeQuery(rel){
		if (rel != undefined) {
			var t = setInterval(function(){
				if(count < rel.length){
					var q = rel[count++];
					Query(q, false);
				} else {
					clearInterval(t);
				}
			}, 250);
		}
	}
	function queryspace(q){
		if (q != undefined) {
			while(q.indexOf("%20")!=-1){
				q = q.replace("%20","+");
			}
			while(q.indexOf(" ")!=-1){
				q = q.replace(" ","+");
			}
			if (q[0] == '+') q = q.replace("+","");
			return q;
		}
		return "";
	}
	function urlquery(q){
		return domain+'?q='+queryspace(encodeURIComponent(q));
	}
	function construct_time(time){
		$('<div>', {'class':"data"}).append(
			$('<div>', {'class':"asr", 'text':'Found in '+Number((time).toFixed(6))+' seconds'})
		).appendTo($("#results"));
		/*<div class="data">
			<div class="asr">Found in 0.00685 seconds</div>
			<div class="asr">Language <i class="idown icon-down-open-mini"></i></div>
		</div>*/
	}
	function construct_icon(url){
		var _domain = url.replace("https://","").replace("http://","");
		_domain = _domain.split("/");

		url = "https://proxy.duckduckgo.com/ip3/"+_domain[0]+".ico";
		return url;
	}
	function get_idvideo_youtube(url){
		var video_id = url.split('v=')[1];
		if (video_id != undefined) {
			var ampersandPosition = video_id.indexOf('&');
			if(ampersandPosition != -1) {
			  video_id = video_id.substring(0, ampersandPosition);
			}
		}
		return video_id;
	}
	function construct_webresult(web){
		for (var i = 0; i < web.length; i++) {
			var field = web[i];
			var url = field.u;
			if(url.indexOf("www.youtube.com/watch?v=") != -1){
				
				$('<div>',{'class':'result'}).append(
					$("<a>",{"href":field.u,"target":"_blank"}).append(
						$("<div>",{"class":"mod-left thumbnail", "html":'<img src="https://i.ytimg.com/vi/'+get_idvideo_youtube(url)+'/hqdefault.jpg"><div class="play"><i class="icon-play"></i></div>'}),
					),
					$("<div>",{"class":"mod-complement"}).append(
						$('<h2>',{'html':'<a href="'+field.u+'" target="_blank"><span>'+field.t+'</span></a>'}),
						$('<p>',{'class':'bread','html':'<a href="'+field.u+'"><img src="'+construct_icon(field.u)+'"><span>'+field.u.replace("https://","").replace("http://","")+'</span></a>'}),
						$('<p>',{'html':field.c})
					)
				).appendTo($("#results"));
			} else {
				$('<div>',{'class':'result'}).append(
					$('<h2>',{'html':'<a href="'+field.u+'" target="_blank"><span>'+field.t+'</span></a>'}),
					$('<p>',{'html':field.c}),
					$('<p>',{'class':'bread','html':'<a href="'+field.u+'"><img src="'+construct_icon(field.u)+'"><span>'+field.u.replace("https://","").replace("http://","")+'</span></a>'})
				).appendTo($("#results"));
			}
		}
	}
	function construct_didyoumean(q, mean){
		$('<div>', {"class":"did-mean"}).append(
			$('<div>', {"class":"mod-left", "html":'<i class="icon-globe"></i>'}),
			$('<div>', {"class":"mod-complement", "html":'<h2>Se muestran resultados de <a href="'+urlquery(q)+'">'+q+'</a></h2><h3>Buscar, en cambio, <a href="'+urlquery(mean)+'">'+mean+'</a></h3>'})
		).appendTo($("#results"));
	}
	function construct_related(related){
		$("<div>",{"class":"related-searches"}).append(
			$("<h2>",{"text":"Related Searches"}),
			$("<ul>")
		).appendTo($("#widgets"));

		for (var i = 0; i < related.length; i++) {
			$('<li>',{'html':'<a href="'+urlquery(related[i])+'">'+boldquery(query, related[i], false)+'</a>'}).appendTo($("#widgets .related-searches ul"));	
		}
		if (related.length > 1)
		$("#widgets .related-searches").show();
	}
	function construct_paginator(query){
		$('<a>', {'href':urlquery(query), 'html':'<div class="paginator">Cargar Más</div>'}).appendTo( $("#results") );
	}
	function doPing(q = "",json=""){
		$.post(api, {"q":q, "action": "first_insert",data:json}/*, function(a){
			console.log("Ping de [",q,"] first_insert : ", a);
		}*/);
	}
	function convertTags(html){
		if(html != undefined){
			while(html.indexOf("\<") != -1){
				html = html.replace("<", "&#x3C;");
			}
			while(html.indexOf(">") != -1){
				html = html.replace(">", "&#x3E;");
			}
		}
		return html;
	}
	function web(q = "", st = true){
		if (!st) return;
		//$apiWeb = "https://www.googleapis.com/customsearch/v1element?key=AIzaSyCVAXiUzRYsML1Pv6RwSG1gunmMikTzQqY&rsz=filtered_cse&num=20&hl=en&prettyPrint=false&source=gcsc&gss=.18&sig=bc136da7ba6658e11a8ffa8e38396a3a&cx=011997700504372929451:rvq2vpf9aqk&q="+q+"&safe=off&cse_tok=AF14hliVsSDa9p_TfUWrH6ILddH4elleow:1539046262133";
		//$apiWeb = "https://www.googleapis.com/customsearch/v1element?key=AIzaSyCVAXiUzRYsML1Pv6RwSG1gunmMikTzQqY&rsz=filtered_cse&num=10&hl=en&prettyPrint=false&source=gcsc&gss=.com&sig=bc136da7ba6658e11a8ffa8e38396a3a&cx=011997700504372929451:rvq2vpf9aqk&q="+q+"&cse_tok=AF14hliGWCAVawD5rCL79M87AC2wtQL_3g:1539219264566";
		//$apiWeb = "https://www.googleapis.com/customsearch/v1element?key=AIzaSyCVAXiUzRYsML1Pv6RwSG1gunmMikTzQqY&rsz=filtered_cse&num=10&hl=en&prettyPrint=false&source=gcsc&gss=.com&sig=bc136da7ba6658e11a8ffa8e38396a3a&cx=011997700504372929451:rvq2vpf9aqk&q="+q+"&cse_tok=AF14hli36sAkFuu8c1uiaM5YwINxASP67Q:1539537497199";
		//$apiWeb = "https://www.googleapis.com/customsearch/v1element?key=AIzaSyCVAXiUzRYsML1Pv6RwSG1gunmMikTzQqY&rsz=filtered_cse&num=10&hl=en&prettyPrint=false&source=gcsc&gss=.com&sig=c891f6315aacc94dc79953d1f142739e&cx=011997700504372929451%3Arvq2vpf9aqk&q="+q+"&cse_tok=AMd4yv0YeDpuyJoC_qdACUJF-gbsOpG2FQ%3A1539733826939";
		//$apiWeb = "https://www.googleapis.com/customsearch/v1element?key=AIzaSyCVAXiUzRYsML1Pv6RwSG1gunmMikTzQqY&rsz=filtered_cse&num=10&hl=en&prettyPrint=false&source=gcsc&gss=.com&sig=c891f6315aacc94dc79953d1f142739e&cx=011997700504372929451%3Arvq2vpf9aqk&q="+q+"&oq="+q+"&cse_tok=AMd4yv08Jl72DgRbcWePWMbqH6VsFoGy1w%3A1540136343113";
		$apiWeb = "https://www.googleapis.com/customsearch/v1element?key=AIzaSyCVAXiUzRYsML1Pv6RwSG1gunmMikTzQqY&rsz=filtered_cse&num=10&hl=en&prettyPrint=false&source=gcsc&gss=.com&sig=c891f6315aacc94dc79953d1f142739e&cx=011997700504372929451:rvq2vpf9aqk&q="+q+"&cse_tok=AMd4yv274gaBHrq5uK1Cafg1HqskOKglRA:1540339378108";
		var arr = [];
		$.get($apiWeb, function(data){
			for (var i = 0; i < data.results.length; i++) {
				var field = data.results[i];
				var url = field.unescapedUrl;
	        	var title = convertTags(field.titleNoFormatting);
	        	var content = convertTags(field.contentNoFormatting);
	        	content = content.replace(/(\r\n\t|\n|\r\t)/gm,"");

				arr.push({u: url, t: title, c: content, s:"cse"});
			}
	        var _json = {"main_page":{"web":arr}};
			if(data.spelling != undefined){
	        	var didYouMean = data.spelling.correctedQuery;
	        	_json["main_page"]["mean"] = didYouMean;
	        	if (st) construct_didyoumean(q, _json["main_page"]["mean"]);
	        	web(didYouMean, false);
	        }
	        if (st) construct_webresult(_json["main_page"]["web"]);

	        suggestQuery(q);
			var t = setInterval(function(){
				if (get("suggestaol") == "true" && get("suggestyoutube") == "true" && get("suggestduck") == "true") {
					var syt   = JSON.parse(get("suggest-youtube"));
					var saol  = JSON.parse(get("suggest-aol"));
					var sduck = JSON.parse(get("suggest-duck"));
					
					var d = [];
					for (var i = 0; i < saol.length; i++) 	d.push(saol[i]);
					for (var i = 0; i < sduck.length; i++) 	d.push(sduck[i]);
					for (var i = 0; i < syt.length; i++) 	d.push(syt[i]);
					d = d.slice(0, 10);
					
					if(st) addRelatedLikeQuery(d); // Si le quito este if va a hacer mucha peticiones a google porque casi casi va a ser infinito ya que la a buscar las queries de las queries
					_json["main_page"]["related"] = d;
			        if(st) construct_related(_json.main_page.related);
			        var json = JSON.stringify(_json.main_page);
			        doPing(q,json);

					clearInterval(t);
				}
			}, 50);
	        
	        /*$.get("https://suggestqueries.google.com/complete/search?client=youtube&ds=yt", {"q":q}, function(data){
				data = data[1];
				var d = [];
				for (var i = 1; i < data.length; i++){
					d.push(data[i][0]);
				}
				if(st) addRelatedLikeQuery(d); // Si le quito este if va a hacer mucha peticiones a google porque casi casi va a ser infinito ya que la a buscar las queries de las queries
				_json["main_page"]["related"] = d;
		        if(st) construct_related(_json.main_page.related);
		        var json = JSON.stringify(_json.main_page);
		        doPing(q,json);
			},"jsonp")
			.fail(function(){
				json = JSON.stringify(_json.main_page);
				doPing(q,json);
			});*/
	    }, "json")
	  	.done(function(data) {
	  		//console.log(data);
	    	//console.log( "second success" );
	  	})
	  	.always(function(data) {
	  		//console.log(data);
	    	//console.log( "finished" );
	  	})
	    .fail(function(){
	    	if (st) {
		    	$("#error").fadeIn();
		    }
	    	//console.log("hay problemas en cse ni siquiera furula");
	    });
	}

	function boldquery(q, data, st = true){		
		return (st) ? data.slice(0, q.length) + '<b>' + data.slice(q.length) + '</b>' : '<b>' + data.slice(0, q.length) + '</b>' + data.slice(q.length);
	}

	
	// Global variables
	function set(name, value){ localStorage.setItem(name, value); }
	function get(name){ return localStorage.getItem(name); }
	function remove(name){ localStorage.removeItem(name); }

	// Suggest
	var enterSuggest = false;

	var arrowSuggest = 0;
	var posSuggest = 0;
	var arrowEnterSuggest = false;
	var pQuerySuggest;

	function remove_construct_suggest(){
		$("#searchInput .suggest").hide();
		$("#searchInput .suggest a").remove();
	}
	function construct_suggest(q, data){
		data = JSON.parse(data);
		if ($("#searchInput .suggest").length) {
			for (var i = 0; i < 8 && i < data.length; i++) {
				
				$("<a>", {"href":urlquery(data[i])}).append(
					$("<div>", {"class":"sug", "html": data[i]/*boldquery(q, data[i])*/})
				).appendTo($("#searchInput .suggest"));
			}
			$("#searchInput .suggest").show();
		}
	}
	function setSuggest(type, data, st = true){
		set("suggest-"+type, JSON.stringify(data));
	}
	function getSuggest(){
		return JSON.parse(get("suggestAll"));	
	}
	function suggestConstruct(q, st = true){
		if (get("sug") != true) {
			set("sug", true);
			suggestQuery(q);
			var t = setInterval(function(){
				if (get("suggestaol") == "true" && get("suggestyoutube") == "true" && get("suggestduck") == "true") {
					var syt   = get("suggest-youtube");
					var saol  = get("suggest-aol");
					var sduck = get("suggest-duck");
					
					if (st){
						//console.log(saol, sduck, syt);
						construct_suggest(q, saol);
					}

					remove("suggestaol");
					remove("suggestduck");
					remove("suggestyoutube");
					clearInterval(t);
					remove("sug");
				}
			}, 50);
		}
	}
	function suggestQuery(q){
		suggestAOL(q);
		suggestDUCK(q);
		suggestYT(q);
	}
	function suggestGeneral(type, url, data){
		$.get(url, data, function(data){
			switch (type) {
				case "aol": 
					res = data.r;
					data = [];
					for (var i = 0; i < res.length; i++)
						data.push(res[i]["k"]);
					set("suggestaol", true);
				break;
				case "youtube":
					res = data[1]; 
					data = [];
					for (var i = 0; i < res.length; i++)
						data.push(res[i][0]);
					set("suggestyoutube", true);
				break;
			}
			setSuggest(type, data);
		}, "jsonp");
	}
	function suggestAOL(q){
		suggestGeneral("aol","https://search.aol.com/sugg/gossip/gossip-us-ura/?nresults=20&output=sd1&dict=en_us_search", {"command":q});
	}
	function suggestYT(q){
		suggestGeneral("youtube","https://suggestqueries.google.com/complete/search?client=youtube&ds=yt", {"q":q});
	}
	function suggestDUCK(q){
		$.ajax({
	        url: 'https://duckduckgo.com/ac/?kl=wt-wt',
	        type: 'GET',
	        dataType: 'jsonp',
	        data : {q:q},
	        jsonpCallback: 'autocompleteCallback',
	        success: function(data) {
	        	res = data;
	        	data = [];
				for (var i = 0; i < res.length; i++)
					data.push(res[i]["phrase"]);
				set("suggestduck", true);
				setSuggest("duck", data);
	        }
	    });
	}

	$(document).on("keyup", "#search form .input input", function(event){
		var si = $("#searchInput .sug");
		si.removeClass("active");
		switch(event.which){
			case 38: // Arriba
				pQuerySuggest = si.eq(posSuggest).text();
				arrowEnterSuggest = true;
				posSuggest--;
				if (arrowSuggest == 40) posSuggest--;
				si.eq(posSuggest).addClass("active");
				if(posSuggest < 0) posSuggest = si.length-1;
				arrowSuggest = 38;
				break;
			case 40: // Abajo
				pQuerySuggest = si.eq(posSuggest).text();
				arrowEnterSuggest = true;
				si.eq(posSuggest).addClass("active");
				posSuggest++;
				if(si.length <= posSuggest) posSuggest = 0;
				arrowSuggest = 40;
				break;
			case 13: // Enter
				if(arrowEnterSuggest){
					$.post(api, {q:pQuerySuggest, action:"check"});
					window.location = domain+"?q="+encodeURI(pQuerySuggest);
				}
				break;
			default:
				arrowEnterSuggest = false;
				remove_construct_suggest();
				var input = $(this).val();
				if (input != "") {
					suggestConstruct(input);
				}
				break;
		}
	});
	$(document).on("mouseenter", "#searchInput .suggest", function(){
		$("#searchInput .sug").removeClass("active");
		posSuggest = 0;
		enterSuggest = true;
	});
	$(document).on("mouseleave", "#searchInput .suggest", function(){
		enterSuggest = false;
	});

	// Search Submit
	$(document).on("submit", "#search form", function(e){
		e.preventDefault();
		if(!arrowEnterSuggest){
			var input = $(this).find(".input input").val();	
			if (input != "") {
				window.location = urlquery(input);
			}
		}
	});

	// Paginator
	var onloaderpaginator = false;
	$(document).on("click", "#results .paginator", function(e){
		e.preventDefault();
		$(this).html('<div class="lds-ellipsis"><div></div><div></div><div></div><div></div></div>');
		setTimeout(function(){
			page++;
			$("#results .paginator").parent().remove();
			if (page <= 7) {
				var data = JSON.parse(get("data"));
				var pg = "page_"+page;
				if (data[pg] != undefined) {
					$("<div>", {"class":"adv-more"}).append(
						$("<div>", {"class":"line"}),
						$("<div>", {"class":"num", "text":page+1})
					).appendTo($("#results"));
					construct_webresult(data[pg]);
					construct_paginator();
				}
			}
			if (data["page_"+parseInt(page+1)] == undefined) {
				$("#results .paginator").parent().remove();
			}
			onloaderpaginator = false;
		}, 250);
	});


    $(document).on("scroll", function(){
  		
    	if ($("#results .paginator").length){
	    	if((100+$("#results .paginator").position().top) < $(document).scrollTop()+$(window).height()){
	    		if (!onloaderpaginator) {
	    			$("#results .paginator").trigger('click');
    				onloaderpaginator = true;
	    		}
	    	}
    	} 	
    	
    });

	// Construct Widget
	$(document).on("click","#wikipedia .head .more", function(){
		$("#apes a").eq(1).trigger('click');	
	});
	$(document).on("click","#widgets .other .back", function(e){
		e.stopPropagation();
		var marginLeft = $(this).parent().find(".items").css("margin-left").replace("px","");
		var width = $(this).parent().width()-2;
		var r = parseInt(marginLeft)+width;
		if (r % width == 0) {
			$(this).parent().find(".items").animate({"margin-left":(r)+"px"});
		}
		if ((marginLeft*-1) == width) $(this).fadeOut();
		if ($(this).parent().find(".next").css("display") == "none")  $(this).parent().find(".next").fadeIn();	
	});
	$(document).on("click","#widgets .other .next", function(e){
		e.stopPropagation();
		var marginLeft = $(this).parent().find(".items").css("margin-left").replace("px","");
		var width = $(this).parent().width()-2;
		var r = parseInt(marginLeft)-width;
		if (r % width == 0) {
			$(this).parent().find(".items").animate({"margin-left":(r)+"px"});
		}
		total = $(this).parent().find(".item-other").length*$(this).parent().find(".item-other").eq(1).outerWidth();
		if (marginLeft == 0) $(this).parent().find(".back").fadeIn();
		if ((total + r) <= width) $(this).fadeOut();	
	});
	function construct_widget(data){
		construct_widget_wikipedia(data.api, data.wiki);
		if(data["Albums"] !== undefined) construct_widget_other("Albums", data.api, data["Albums"]);
		if(data["People also search for"] !== undefined) construct_widget_other("People also search for", data.api, data["People also search for"]);
		if(data["Cast"] !== undefined) construct_widget_other("Cast", data.api, data["Cast"]);
		//construct_widget_movies();
		if(data.wiki.v !== undefined) construct_widget_urls(data.wiki.v);
	}
	function construct_widget_wikipedia(api, wiki){
		$("<div>",{"id":"wikipedia"}).appendTo($("#widgets"));
		$("<div>",{"class":"head"}).appendTo($("#wikipedia"));
		var data;
		if (wiki.g == undefined || wiki.g.b == undefined) { //con una o ninguna imagen
			if (wiki.i.y != undefined) { // video
				$("<div>",{"class":"video","html":'<iframe width="100%" height="250" src="https://www.youtube.com/embed/'+wiki.i.y+'" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>'}).appendTo($("#wikipedia .head"));
				data = '<div class="video">';
					data += '<iframe width="100%" height="250" src="https://www.youtube.com/embed/'+wiki.i.y+'" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>';
				data += '</div>'; 
			} else { // unico
				data = '<div class="mod-right thumbnail"><img src="'+"https://"+api.d+"/"+api.o+"/"+wiki.g.a.u+'"></div>';
				data += '<div class="mod-complement" style="margin:10px;">';
						data += '<h2><a href="#">'+wiki.i.t+'</a></h2>';
						data += '<h4>'+wiki.i.w+'</h4>';
						data += '<a href="'+wiki.i.z+'" target="_blank"><button>Go Wikipedia <i class="icon-right-open-mini"></i></button></a>';
				data += '</div>';
			}
			$("#wikipedia .head").html(data);
		} else { // con muchas imagenes
			var up = 0,button = 0;
			
			// insercion de imagenes secundarias
			for (var i = 0; i < wiki.g.b.u.length; i++) {
				$("<div>",{"class":"item-image"}).append(
					$("<img>",{"src":"https://"+api.d+"/"+api.o+"/"+wiki.g.b.u[i]+'/t_140x140',"height":wiki.g.b.h[i],"width":wiki.g.b.w[i]})
				).appendTo($("#wikipedia .head"));
				if (up == 0){ 
					up = wiki.g.b.h[i];
				} else {
					if (wiki.g.b.h[i-1] != wiki.g.b.h[i]){
						button = wiki.g.b.h[i];
					}
				}
			}
			$("<a>",{"class":"more","text":"More images"}).appendTo($("#wikipedia .head"));

			// insercion de imagen principal
			var base = parseInt(up)+parseInt(button)+2;
			$("<div>",{"class":"item-image", "style":"height:"+base+"px"}).append(
				$("<img>",{"src":"https://"+api.d+"/"+api.p+"/"+wiki.g.a.i+"/"+wiki.g.k+"/https://"+api.d+"/"+api.o+"/"+wiki.g.a.u+"/t_500x300"})
			).prependTo($("#wikipedia .head"));
			var real = $("#wikipedia .head .item-image").eq(0).find("img").outerHeight();
			if (real != 0) {
				var m = ((real > base)? (real-base) : (base-real))/2;
				$("#wikipedia .head .item-image").eq(0).find("img").css({"margin-top":"-"+m+"px"});
			}


			// move div
			$("#result-images-example").insertAfter($("#results .result").eq(4));
		}
		//Insercion de datos
		data = '<div class="data">';
			if (wiki.g == undefined || wiki.g.b != undefined || wiki.i.y != undefined) {
				data += '<div class="title">';
					data += '<div class="mod-right"><a href="'+wiki.i.z+'" target="_blank"><button>Go Wikipedia <i class="icon-right-open-mini"></i></button></a></div>';
					data += '<div class="mod-complement">';
						data += '<a href="'+wiki.i.z+'"><h2>'+wiki.i.t+'</h2></a>';
						if (wiki.i.w != undefined && wiki.i.w != "general") {
							data += '<h4>'+wiki.i.s.d+'</h4>';
						} else if(wiki.i.s != undefined){
							data += '<h4>'+wiki.i.s+'</h4>';
						}
					data += '</div>';
				data += '</div>';
			}
			if (wiki.i.w == "movie" && wiki.i.r != undefined) {
				if (wiki.i.r["IMDb"] != undefined && wiki.i.r["Rotten Tomatoes"] != undefined){
					data += '<div class="rs mod">';
						if (wiki.i.r["IMDb"] != undefined) {
							data += '<div class="d">'+wiki.i.r["IMDb"].v+' <a href="'+wiki.i.r["IMDb"].u+'" target="_blank">IMDb</a></div>';
						}
						if (wiki.i.r["Rotten Tomatoes"] != undefined) {
							data += '<div class="d"><img src="https://staticv2-4.rottentomatoes.com/static/images/icons/favicon.ico"><span>'+wiki.i.r["Rotten Tomatoes"].v+'</span><a href="'+wiki.i.r["Rotten Tomatoes"].u+'" target="_blank">Rotten Tomatoes</a></div>';
						}
					data += '</div>';
				}
			}
			if (wiki.i.d != undefined) {
				data += '<div class="url">';
					data += '<i class="icon-globe"></i><a href="'+wiki.i.d+'" target="_blank">'+wiki.i.d.replace("https://","").replace("http://","")+'</a>';
				data += '</div>';
			}
			data += '<div class="info">';
				data += '<p>'+wiki.i.e+'</p>';
				data += '<ul></ul>';
			data += '</div>';
		data += '</div>';
		$("#wikipedia .head").after(data);


		//Insercion  de la lista de datos
		if (wiki.j != undefined) {
			for ( property in wiki.j.l ) {
				$("<li>", {"html":'<b>'+property+':</b> '+wiki.j.l[property]}).appendTo($("#wikipedia .data .info ul"));
			}
		}
	}
	function construct_widget_other(name, api, other){
		var d = '';
		for (var i = 0; i < other.u.length; i++) {
			var q = (name == "Albums")? "Album ": " ";
			q += other.t[i];
			d += '<a href="'+urlquery(q)+'"><div class="item-other"><div class="thumbnail"><img src="'+"https://"+api.d+"/"+api.o+"/"+other.u[i]+'/t_140x140"></div><div class="name">'+other.t[i]+'</div></div></a>';
		}
		$("<div>",{"class":"other"}).append(
			$("<h3>",{"text":name}),
			$("<div>",{"class":"back hide","html":'<i class="icon-left-open"></i>'}),
			$("<div>",{"class":"next","html":'<i class="icon-right-open"></i>'}),
			$("<div>",{"class":"items","html":d})
		).appendTo($("#widgets"));
	}
	function construct_widget_movies(){
		/*
		<div class="other">
				<h3>People also search for</h3>
				<div class="back"><i class="icon-left"></i></div>
				<div class="next"><i class="icon-left"></i></div>
				<div class="items">
					<?php for($i = 0; $i < 10;$i++){?>
					<a href="#">
						<div class="item-other">
							<div class="thumbnail"><img src="https://s.yimg.com/zb/imgv1/09570fff-86cb-379e-979e-f9ce2f99fe37/t_500x300"></div>
							<div class="name">Deadpool</div>
							<div class="time">2016</div>
						</div>
					</a>
					<?php } ?>
				</div>
			</div>
		*/
	}
	function get_domain(url, st = false){
		url = url.split("/")[2].split(".");
		var _url = url[url.length-2];
		if (st) 
			_url += "."+url[url.length-1];
		return _url;
	}
	function construct_widget_urls(url){
		var d = '';
		for ( property in url ) {
			var sd = get_domain(url[property]);
			if (sd != "vudu" && sd != "imdb" && sd != "rottentomatoes" && sd != "netflix")
			d += '<a href="'+url[property]+'" target="_blank"><div class="item-other"><div class="io '+sd+'"><i class="icon-'+sd+'"></i></div></div></a>';	
		}
		$("<div>", {"class":"other urls","html":d}).appendTo("#widgets");
	}
    
    // Function
    function float_precision(number, n = 2) {
    	num = String(number).split(".");
    	if (num.length == 2) {
    		return (parseFloat(number).toPrecision(num[0].length+n))
    	} else 
    		return number;
	}
    function justify_images(where, json, f = function(){}){
    	if (where.length) {
	    	var maxWidth = (json.maxWidth != undefined) ? json.maxWidth : where.outerWidth();
	    	var current = json.margin;
	    	margin = json.margin/2;
	    	where.css({"padding":margin});

	    	var index = 0;
	    	var _json = [];
	    	var _json_;
	    	var l = 0;
	    	for(var i = 0; i < json.images.length; i++) {
				var mainData       = json.mainData(json.images[i]);
				var r_width        = mainData.w*json.height/mainData.h;
				var r_height       = r_width*mainData.h/mainData.w;
				var marginTopImage = (json.height-r_height)/2;
				current += parseFloat(r_width)+json.margin;
				json.images[i]["urlThumbnail"]   = mainData.urlThumbnail;
				json.images[i]["rWidth"]         = float_precision(r_width);
				json.images[i]["rHeight"]        = float_precision(r_height);
				json.images[i]["marginTopImage"] = float_precision(marginTopImage);
				json.images[i]["realUrl"]        = (mainData.realUrl != undefined) ? mainData.realUrl : mainData.urlThumbnail;
				json.images[i]["maxHeight"]      = json.height;
				json.images[i]["midMargin"]      = margin;
	    				
	    		if (current > maxWidth) {
	    			_json_ = [];
	    			rW = parseFloat(maxWidth)-(parseFloat(current)-(parseFloat(r_width)+json.margin));
					rrW = (parseFloat(r_width)+json.margin)/2;
	    			mk = rW-rrW;
	    			if (mk >= 0) { //Achicar
	    				for (var o = index; o <= i; o++) {
		    				_json_.push(json.images[o]);
		    			}
	    				current = json.margin;
	    				index = i+1;
	    			} else{ // Agrandar
	    				for (var o = index; o < i; o++) {
		    				_json_.push(json.images[o]);
		    			}
	    				current = json.margin+parseFloat(r_width);
	    				index = i;
	    			}
	    			_json.push(_json_);
	    			l++;
	    		}
	    		if (json.maxLines != undefined)
	    			if (l >= json.maxLines) break;
	    	}
	    	for(var i = 0; i < _json.length; i++) {
	    		ww = 0;
				for (var o = 0; o < _json[i].length; o++) {
					ww += parseFloat(_json[i][o]["rWidth"])
					ww += json.margin;
				}
				ww += json.margin;
				var w = parseFloat(parseFloat(parseFloat(ww)-parseFloat(maxWidth))/parseFloat(_json[i].length));
				for (var o = 0; o < _json[i].length; o++) {
    				_json[i][o]["rWidth"] = float_precision(parseFloat(_json[i][o]["rWidth"])-w);
    			}
	    	}
	    	var _i = 0;
	    	for(var i = 0; i < _json.length; i++) {
	    		for(var o = 0; o < _json[i].length; o++) {
	    			if (json.template == undefined) {
		    			$("<div>", {"class":"photo-container","style":"height:"+_json[i][o].rHeight+"px;margin:"+margin+"px;"}).append(
		    				$("<img>", {"src":json.mainData(_json[i][o]).url, "style":"height:"+_json[i][o].rHeight+"px;width:"+_json[i][o].rWidth+"px;margin-top:"+_json[i][o].marginTopImage+"px" })
		    			).appendTo(where);
	    			} else {
	    				_json[i][o]["index"] = _i;
	    				where.append(json.template(_json[i][o]));
	    			}
	    			_i++;
	    		}
	    	}
			f();
			return json.images;
    	} else {
    		console.log("No existe este lugar");
    	}
    }

    function construct_images(_photos){
        var photos = [];
        for (var k = 0;k < _photos.f.length; k++) {
        	photos.push({
        		"f":_photos.f[k],
        		"h":_photos.h[k],
        		"i":_photos.i[k],
        		"s":_photos.s[k],
        		"t":_photos.t[k],
        		"th":_photos.th[k],
        		"tw":_photos.tw[k],
        		"u":_photos.u[k],
        		"w":_photos.w[k],
        	});
        }
        var imgs = justify_images($('#result-images'), {
    		images : photos,
            height: 160,
            maxWidth: $(document).outerWidth(),
            margin: 10,
            mainData: function(photo){
            	return {realUrl:photo.f, urlThumbnail:'https://tse1.mm.bing.net/th?id=OIP.'+photo.i+'&h=160'+photo.f, h: photo.h, w:photo.w};
            },
            template:function(photo){
            	return '<div class="photo-container" data-index="'+photo.index+'" data-type="self" data-realwidth="'+photo.w+'" data-realheight="'+photo.h+'" style="height:'+photo.maxHeight+'px;margin:'+photo.midMargin+'px"><img class="b-lazy" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAQAAAAEACAQAAAD2e2DtAAAABGdBTUEAALGPC/xhBQAAAAFzUkdCAK7OHOkAAAAgY0hSTQAAeiYAAICEAAD6AAAAgOgAAHUwAADqYAAAOpgAABdwnLpRPAAAAAJiS0dEAP7wiPwpAAAACXBIWXMAAA3WAAAN1gGQb3mcAAAAu0lEQVR42u3SQREAIRDAsOP8q0UBFvgtM00U9NH1PWlPB2T80wHMMkCcAeIMEGeAOAPEGSDOAHEGiDNAnAHiDAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABw7wBj/wEMQ93nRgAAACV0RVh0ZGF0ZTpjcmVhdGUAMjAxMS0wNC0wN1QxMzo1MTo0NyswMzowMJTIG8YAAAAldEVYdGRhdGU6bW9kaWZ5ADIwMTEtMDQtMDdUMTM6NTE6NDcrMDM6MDDllaN6AAAAAElFTkSuQmCC" data-src="'+photo.urlThumbnail+'" style="height:'+photo.rHeight+'px;width:'+photo.rWidth+'px;margin-top:'+photo.marginTopImage+'px" ></div>';
            }
    	}, function(){var bLazy = new Blazy();});
    	set("images_self", JSON.stringify(imgs));
        
    	justify_images($('#result-images-example'), {
    		images : photos,
            height: 100,
            maxLines: 2,
            margin: 1,
            mainData: function(photo){
            	return {realUrl: photo.f, urlThumbnail:'https://tse1.mm.bing.net/th?id=OIP.'+photo.i+'&h=160'+photo.f, h: photo.h, w:photo.w};
            },
            template:function(photo){
            	return '<div class="photo-container" style="height:'+photo.maxHeight+'px;margin:'+photo.midMargin+'px"><img class="b-lazy" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAQAAAAEACAQAAAD2e2DtAAAABGdBTUEAALGPC/xhBQAAAAFzUkdCAK7OHOkAAAAgY0hSTQAAeiYAAICEAAD6AAAAgOgAAHUwAADqYAAAOpgAABdwnLpRPAAAAAJiS0dEAP7wiPwpAAAACXBIWXMAAA3WAAAN1gGQb3mcAAAAu0lEQVR42u3SQREAIRDAsOP8q0UBFvgtM00U9NH1PWlPB2T80wHMMkCcAeIMEGeAOAPEGSDOAHEGiDNAnAHiDAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABw7wBj/wEMQ93nRgAAACV0RVh0ZGF0ZTpjcmVhdGUAMjAxMS0wNC0wN1QxMzo1MTo0NyswMzowMJTIG8YAAAAldEVYdGRhdGU6bW9kaWZ5ADIwMTEtMDQtMDdUMTM6NTE6NDcrMDM6MDDllaN6AAAAAElFTkSuQmCC" data-src="'+photo.urlThumbnail+'" style="height:'+photo.rHeight+'px;width:'+photo.rWidth+'px;margin-top:'+photo.marginTopImage+'px" ></div>';
            }
    	}, function(){
    		$("#result-images-example").css({"margin-top":"19px","border-bottom":"1px solid #ededec","paddingBottom":"20px"});
    	}, function(){var bLazy = new Blazy();});
    	var bLazy = new Blazy();
    }
    function otherimages(query){
    	$.ajax({
            url : 'https://api.flickr.com/services/rest/?jsoncallback=?',
            method: 'get',
            data : {
                method : 'flickr.photos.search',
                api_key : '2b76793b6787a09c14929811d2cef67e',
                text : query,
                sort : 'interestingness-desc',
                format : 'json',
                extras : 'url_o,url_s,url_l',
                per_page : 100,
                page: 1
            },
            dataType: 'json',
            success : function(data){
            	var d = [];
            	var _data = data.photos.photo;
            	for (var i = 0; i < _data.length; i++) {
            		if (_data[i]["url_o"] != undefined || _data[i]["url_l"] != undefined) {
	            		d.push({
	            			"u":  (_data[i]["url_o"] != undefined )?_data[i]["url_o"]:_data[i]["url_l"],
	            			"h":  (_data[i]["height_o"] != undefined )?_data[i]["height_o"]:_data[i]["height_l"],
	            			"w":  (_data[i]["width_o"] != undefined )?_data[i]["width_o"]:_data[i]["width_l"],
	            			"tu": _data[i]["url_s"],
	            			"th": _data[i]["height_s"],
	            			"tw": _data[i]["width_s"],
	            		});
	            	}
            	}
        		construct_otherimages(d);
            }
        })
    }
    function construct_otherimages(photo){
    	var imgs = justify_images($('#result-images'), {
    		images : photo,
            height: 160,
            maxWidth: $(document).outerWidth(),
            margin: 10,
            mainData: function(photo){
            	return {realUrl:photo.u, urlThumbnail:photo.tu, w: photo.tw, h: photo.th};
            },
            template:function(photo){
            	return '<div class="photo-container" data-index="'+photo.index+'" data-type="flickr" data-realwidth="'+photo.w+'" data-realheight="'+photo.h+'" style="height:'+photo.maxHeight+'px;margin:'+photo.midMargin+'px"><img class="b-lazy" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAQAAAAEACAQAAAD2e2DtAAAABGdBTUEAALGPC/xhBQAAAAFzUkdCAK7OHOkAAAAgY0hSTQAAeiYAAICEAAD6AAAAgOgAAHUwAADqYAAAOpgAABdwnLpRPAAAAAJiS0dEAP7wiPwpAAAACXBIWXMAAA3WAAAN1gGQb3mcAAAAu0lEQVR42u3SQREAIRDAsOP8q0UBFvgtM00U9NH1PWlPB2T80wHMMkCcAeIMEGeAOAPEGSDOAHEGiDNAnAHiDAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABw7wBj/wEMQ93nRgAAACV0RVh0ZGF0ZTpjcmVhdGUAMjAxMS0wNC0wN1QxMzo1MTo0NyswMzowMJTIG8YAAAAldEVYdGRhdGU6bW9kaWZ5ADIwMTEtMDQtMDdUMTM6NTE6NDcrMDM6MDDllaN6AAAAAElFTkSuQmCC" data-src="'+photo.tu+'" style="height:'+photo.rHeight+'px;width:'+photo.rWidth+'px;margin-top:'+photo.marginTopImage+'px" ></div>';
            }
    	}, function(){var bLazy = new Blazy();});
    	set("images_flickr", JSON.stringify(imgs));
    }
    $(document).on("mouseenter", "#result-images .photo-container,#img-related .photo-container", function(){
    	var ele = $(this);
    	var width = ele.attr("data-realWidth");
    	var height = ele.attr("data-realHeight");
    	$("<div>", {"class":"plus"}).append(
    		$("<div>", {"class":"icon"}).append(
    			$("<i>", {"class":"icon-plus-circled"})
    		),
    		$("<div>", {"class":"size", "text":width+" x "+height})
    	).hide().appendTo(ele).fadeIn(250);
    });
    $(document).on("mouseleave", "#result-images .photo-container,#img-related .photo-container", function(){
    	var ele = $(this);
    	ele.find(".plus").fadeOut(150, function(){
    		ele.find(".plus").remove();
    	});
    });

    $(document).on("mouseenter", "#show-photo", function(){
    	$(this).addClass("enter-viewer-photo");	
    });
    $(document).on("mouseleave", "#show-photo", function(){
    	$(this).removeClass("enter-viewer-photo");
    });

    function resizeImage(ih, iw){
		var imgwidth    = ($(window).outerWidth() - 160 - $("#show-photo .details").outerWidth());
		var imgheight   = ($(window).outerHeight() - 160);
		
		var imagewidth, imageheight;
		
		var _h;
		if (ih > imgheight) { //el alto de imagen es mayor que el ancho del fondo
			_h = ih;
		} else {
			_h = imgheight;
		}
		imagewidth  = parseFloat(imgheight*iw/_h);
		imageheight = parseFloat(ih*imagewidth/iw);
		console.log("img: ", imagewidth, imageheight);
		imagewidth  = imagewidth/2; 
		imageheight = imageheight/2; 
		imgwidth    = imgwidth/2;
		imgheight   = imgheight/2;
		
		var rheight = imgheight - imageheight;
		var rwidth  = imgwidth - imagewidth;

		$("#show-photo .image img").attr("style", "");
		
		//console.log(imagewidth, imgwidth);
		//if (imagewidth == imgwidth) {
			if ( (imgheight/3) > rheight ) {
	    		$("#show-photo .image img").css({"max-width":"fit-content"});
			}
		//}
		//console.log(imageheight, imgheight);
		//if (imageheight == imgheight) {
			if ( (imgwidth/3) > rwidth ) {
	    		$("#show-photo .image img").css({"max-height":"fit-content"});
			}
		//}
    }
    function resizeImg(photo){
		var height = $(window).outerHeight() - 160;
		var height_data = height/*-$("#show-photo .data").outerHeight()*/;
		$("#show-photo .details .related").css({"height":height_data+"px"});
		resizeImage(photo.h, photo.w);
    }
    function showImage(type, index, data){
    	var photo = data[index];
    	var realUrl = photo.realUrl;
    	var domain = get_domain(realUrl, true);
    	var title = photo.t;
		if (!$("#background-black").length) {
			$(window).scrollTop( 0 );
	    	$("html").css({"overflow":"hidden"});
	    	
	    	var html;
	    	html = '<div id="background-black"><div class="close"><i class="icon-cancel"></i></div>';
		    	html += '<div id="show-photo">';
		    		html += '<div class="details mod-right">';
		    			/*html += '<div class="data">';
			    			if (photo.t != undefined)
			    			html += '<a href="'+photo.realUrl+'" target="_blank"><div class="title">'+photo.t+'</div></a>';
			    			html += '<a href="'+photo.realUrl+'"><div class="domain">'+get_domain(photo.realUrl, true)+'</div></a>';
						html += '</div>';*/
			    		html += '<div class="related scrollbar">';
		    				html += '<div id="img-related">';
		    				html += '<h2>Related images</h2>';
		    				html += '</div>';
			    		html += '</div>';
		    		html += '</div>';
		    		html += '<div class="image mod-complement">';
		    			if (photo.t != undefined){
			    		html += '<div class="tl">';
		    				html += '<a href="'+photo.realUrl+'" target="_blank"><div class="title">'+photo.t+'</div></a>';
			    			html += '<a href="'+photo.realUrl+'"><div class="domain">'+get_domain(photo.realUrl, true)+'</div></a>';
			    		html += '</div>';
		    			}
			    		html += '<img src="' + photo.realUrl + '">'
		    			html += '<a href="#"><div class="download"><div class="second mod-right">';
		    				/*html += '<select>';
		    					html += '<option value="0">1980 x 1687</option>';
		    					html += '<option value="0">580 x 465</option>';
		    					html += '<option value="0">180 x 187</option>';
		    				html += '</select>';*/
		    			html +='</div><div class="first mod-complement"><i class="icon-download-alt"></i> Download</div></div></a>';
		    		html += '</div>';
		    	html += '</div>';
		    html += '</div>';
	    	$("body").append(html);
	    	switch(type){
	    		case "self":
	    			justify_images($('#img-related'), {
			    		images : data,
			            height: 115,
			            margin: 5,
			            mainData: function(photo){
			            	return {realUrl:photo.f, urlThumbnail:'https://tse1.mm.bing.net/th?id=OIP.'+photo.i+'&h=160'+photo.f, h: photo.h, w:photo.w};
			            },
			            template:function(photo){
			            	return '<div class="photo-container" data-index="'+photo.index+'" data-type="self" data-realwidth="'+photo.w+'" data-realheight="'+photo.h+'" style="height:'+photo.maxHeight+'px;margin:'+photo.midMargin+'px"><img src="'+photo.urlThumbnail+'" style="height:'+photo.rHeight+'px;width:'+photo.rWidth+'px;margin-top:'+photo.marginTopImage+'px" ></div>';
			            }
			    	});
	    		break;
	    		case "flickr":
	    			justify_images($('#img-related'), {
			    		images : data,
			            height: 115,
			            margin: 5,
			            mainData: function(photo){
			            	return {realUrl:photo.u, urlThumbnail:photo.tu, w: photo.tw, h: photo.th};
			            },
			            template:function(photo){
			            	return '<div class="photo-container" data-index="'+photo.index+'" data-type="flickr" data-realwidth="'+photo.w+'" data-realheight="'+photo.h+'" style="height:'+photo.maxHeight+'px;margin:'+photo.midMargin+'px"><img src="'+photo.urlThumbnail+'" style="height:'+photo.rHeight+'px;width:'+photo.rWidth+'px;margin-top:'+photo.marginTopImage+'px" ></div>';
			            }
			    	});
	    		break;
	    	}
    	} else {
    		//<div class="lds-ring"><div></div><div></div><div></div><div></div></div>
    		$("#show-photo .image img").remove();
			$("#show-photo .image").append('<img src="'+photo.realUrl+'">').hide().fadeIn();
			$("#show-photo .image a").attr("href", photo.realUrl);
    		$("#show-photo .image .title").text(title);
    		$("#show-photo .image .domain").text(domain);

    	}
    	resizeImg(photo);
    }
    var show_viewer = false;
    function hideImage(){
    	$("html").css({"overflow-y":"scroll"});
    	$("#background-black").fadeOut(250, function(){
    		$(this).remove();
    	});
    	show_viewer = false;
    }
    $(document).on("click", "#background-black .close", function(){
    	hideImage();
    });
    $(document).on("click", "#result-images .photo-container, #img-related .photo-container", function(){
    	setTimeout(function(){
    		show_viewer = true;
    	}, 250);
    	var index = $(this).attr("data-index");
    	var type = $(this).attr("data-type");
    	var data = JSON.parse(get("images_"+type));
    	set("image_type", "images_"+type);
    	set("image_index", index);

    	showImage(type, index, data);

    });

    function number_format(n){
		return String(n).replace(/(.)(?=(\d{3})+$)/g,'$1.');
    }
    //Construct Videos
    function construct_videos(data){
    	if (data != null) {
	    	for (var i = 0; i < data.t.length; i++) {
				var title  = data.t[i];
				var author = data.c[i];
				var time   = data.d[i];
				var views  = data.h[i];
				var id     = data.i[i];

				$("<div>", {"class":"item-video"}).append(
					$("<div>", {"class":"item", "data-id":id}).append(
						$("<a>", {"href":"https://www.youtube.com/watch?v="+id, "html":'', "target":"_blank"}).append(
							$("<div>", {"class":"thumbnail"}).append(
								$("<img>", {"src":"https://i.ytimg.com/vi/"+id+"/hqdefault.jpg"}),
								$("<div>", {"class":"play", "html":'<i class="icon-play"></i>'}),
								$("<div>", {"class":"time", "text":time})
							)
						),
						$("<div>", {"class":"wrapper"}).append(
							$("<div>", {"class":"mod-complement"}).append(
								$("<a>", {"href":"https://www.youtube.com/results?search_query="+author, "html":''}).append(
									$("<div>", {"class":"author", "text":author})
								),
								$("<div>", {"class":"views", "text":number_format(views)})
							),
							$("<a>", {"href":"https://www.youtube.com/watch?v="+id, "html":''}).append(
								$("<div>", {"class":"title", "text":title})
							)
						)
					)
				).appendTo($("#result-videos"));
				if (i == 0) {
					$("#results .result:nth-child(4)").after(
		    			$("<div>",{"class":"result"}).append(
		    				$("<div>",{"class":"videos"})
		    			)
	    			);
				}
				if (i < 3) {
    				$("<div>", {"class":"video"}).append(
	    				$("<div>", {"class":"item-video"}).append(
	    					$("<a>", {"href":"https://www.youtube.com/watch?v="+id, "html":'', "target":"_blank"}).append(
			    				$("<div>", {"class":"thumbnail"}).append(
									$("<img>", {"src":"https://i.ytimg.com/vi/"+id+"/hqdefault.jpg"}),
			    					$("<div>", {"class":"play", "html":'<i class="icon-play"></i>'})
			    				)
		    				),
		    				$("<div>", {"class":"data"}).append(
		    					$("<div>", {"class":"mod-complement"}).append(
		    						$("<div>", {"class":"author", "text":author}),
		    						$("<div>", {"class":"views", "text":number_format(views)})
		    					),
		    					$("<a>", {"href":"https://www.youtube.com/watch?v="+id, "html":'', "target":"_blank"}).append(
		    						$("<div>", {"class":"title", "text":title})
		    					)
		    				)
	    				)
	    			).appendTo($("#results .result:nth-child(5) .videos"));
				}
	    	}
	    }
    }
    function showGraphics(elem, size){
    	$("#results, #widgets").hide();
    	if (elem > 0) {
	    	switch(elem){
	    		case 1:
	    			$("#graphics #result-videos").hide();
    				$("#graphics #result-images").show();
	    		break;
	    		case 2:
    				$("#graphics #result-images").hide();
	    			$("#graphics #result-videos").show();
	    		break;
	    	}
	    	$("#graphics").show();
	    }
    }
    function hideGraphics(){
    	$("#graphics").fadeOut(250,function(){
    		$("#graphics #result-images").hide();
    		$("#graphics #result-videos").hide();	
    		$("#results, #widgets").show();
    	});
    }
	$(document).on("click", "#apes a", function(e){
		e.preventDefault();
		var index = $(this).index();
		var size;
		$(window).scrollTop( 0 );
		switch(index){
			case 0: // Web
				hideGraphics();		
			break;
			case 1: // Imagenes
				var bLazy = new Blazy();
				size = $("#graphics #result-images").outerHeight()-16;
			break;
			case 2: // Videos
				size = $("#graphics #result-videos").outerHeight();
			break;	
		}
		showGraphics(index, size);
		$("#apes a").removeClass("active");
		$(this).addClass("active");
	});
	$(document).on("click", "#result-images-example", function(){
		$("#apes a").eq(1).trigger('click');
	});
	$(window).resize(function(){
		if($("#show-photo").length){
    		var data = JSON.parse(get(get("image_type")));
    		var index = get("image_index");
			resizeImg(data[index]);
		}
    });
	$(document).on("click", function(){
		if (!enterSuggest) {
			remove_construct_suggest();
		}
		if (show_viewer) {
			if (!$("#show-photo").hasClass("enter-viewer-photo")) {
				$("#background-black .close").trigger("click");
			}
		}
	});
});
/*
- pensar en que hacer cuando el cse no funcione.
*/