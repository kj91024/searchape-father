<?php
//$ip = "10.100.140.139";

$index = "http://$ip/";
$_query = urldecode($_GET["q"]);
$query = trim($_query);
$dquery = "http://".$ip."?q=".str_replace(" ", "+", $query);
if ($query != $_query) header("Location: ".$dquery);
if(!$query || empty($query)) header("Location: ".$index);
?>
<!DOCTYPE html>
<html>
<head>
	<title><?=$query?> - SearchAPE</title>
	<meta charset="utf-8">
	<link rel="stylesheet" type="text/css" href="assets/css/global.css">
	<link rel="stylesheet" type="text/css" href="assets/css/style.css">
	<link rel="stylesheet" type="text/css" href="assets/css/searchape-embedded.css">
</head>
<body>
	<header>
		<div class="main">
			<a href="<?=$index?>">
				<div class="mod-left">
					<div class="logo mod-left">
						<svg viewBox="0 0 490 525" style="width:45px;float:right;padding-left: 2px;">
		                    <path fill="#e45828" d="M325 85c1 12-1 25-5 38-8 29-31 52-60 61-26 8-54 14-81 18-37 6-26-37-38-72l-4-4c0-17-9-33 4-37l33-4c9-2 9-21 11-30 1-7 3-14 5-21 8-28 40-42 68-29 18 9 36 19 50 32 13 11 16 31 17 48z"></path>
		                    <path fill="none" stroke="#e45828" stroke-width="24" stroke-linecap="round" stroke-miterlimit="10" d="M431 232c3 15 21 19 34 11 15-9 14-30 5-43-12-17-38-25-59-10-23 18-27 53-21 97s1 92-63 108"></path>
		                    <path fill="#e45828" d="M284 158c35 40 63 85 86 133 24 52-6 113-62 123-2 0-4 1-6 1-53 9-101-33-101-87V188l83-30z"></path>
		                    <path fill="#ffeac8" d="M95 152c-3-24 13-57 46-64l27-5c9-2 16-19 17-28l3-15 20-3c44 14 42 55 18 69 22 0 39 26 32 53-5 18-20 32-39 36-13 3-26 5-40 8-50 8-80-14-84-51z"></path>
		                    <path fill="#e45828" d="M367 392c-21 18-77 70-25 119h-61c-27-29-32-69 1-111l85-8z"></path>
		                    <path fill="#e45828" d="M289 399c-21 18-84 62-32 111h-61c-37-34-5-104 43-134l50 23z"></path>
		                    <path fill="#fdd193" d="M185 56l3-15 20-3c25 8 35 25 35 41-12-18-49-29-58-23z"></path>
		                    <path fill="#2379ce" d="M190 34c8-28 40-42 68-29 18 9 36 19 50 32 10 9 14 23 16 37L187 46l3-12z"></path>
		                    <path fill="#ce4a1c" d="M292 168c0 0 0 201 0 241s20 98 91 85l-16-54c-22 12-31-17-31-37 0-20 0-108 0-137S325 200 292 168z"></path>
		                    <path fill="#ffeac8" d="M284 79c11-9 23-17 35-23 25-12 54 7 59 38v1c4 27-13 51-36 53-12 1-25 1-37 0-22-1-39-27-32-52v-1c2-6 6-12 11-16z"></path>
		                    <path fill="#e45828" d="M201 203s0 84-95 140l22 42s67-25 89-86-16-96-16-96z"></path>
		                    <path fill="#2379ce" d="M224 54l-67-14c-10-2-13-15-5-21s18-6 26 0l46 35z"></path>
		                    <circle fill="#444444" cx="129" cy="161" r="12"></circle>
		                    <circle fill="#444444" cx="212" cy="83" r="7"></circle>
		                    <circle fill="#444444" cx="189" cy="79" r="7"></circle>
		                    <path fill="#ffeac8" d="M383 493c11-3 19-8 25-13 7-10 4-16-5-20 8-9 2-22-8-18 1-1 1-2 1-3 3-9-9-15-15-8-3 4-8 7-13 9l15 53z"></path>
		                    <path fill="#fdd193" d="M252 510c5 6 0 15-9 15h-87c-10 0-16-8-13-15 5-12 21-19 36-16l73 16z"></path>
		                    <ellipse transform="rotate(19.126 278.35 14.787)" fill="#2379ce" cx="278" cy="15" rx="9" ry="7"></ellipse>
		                    <path fill="#ffeac8" d="M341 510c5 6 0 15-9 15h-87c-10 0-16-8-13-15 5-12 21-19 36-16l73 16z"></path>
		                    <path fill="#fdd193" d="M357 90c-12-19-35-23-55-11-19 12-25 32-13 52"></path>
		                    <path fill="#333333" d="M110 427l21-9c5-2 7-8 5-13l-42-94c-3-6-9-9-15-6l-11 5c-6 2-9 9-7 15l36 97c2 5 8 7 13 5z"></path>
		                    <ellipse transform="rotate(173.3 233.455 334.51)" fill="#2279ce" cx="400" cy="425" rx="75" ry="75"></ellipse>
		                    <ellipse transform="rotate(173.3 233.455 334.51)" fill="#fff" cx="400" cy="425" rx="58" ry="58"></ellipse>
		                    <ellipse transform="rotate(133.3 233.455 334.51)" fill="#f1f1f1" cx="280" cy="525" rx="18" ry="20"></ellipse>
		                    <!--<path fill="#B0BEC5" d="M37 278l41-17c11-4 22-5 33-1 5 2 10 4 14 6 6 3 4 11-3 11-9 0-18 1-26 3l2 12c1 6-2 11-8 13l-36 15c-5 2-10 1-14-2l-9-7-2 17c0 2-2 4-4 5l-3 1c-3 1-7 0-8-3L1 300c-1-3 0-7 4-9l4-2c2-1 5 0 7 1l12 10 1-11c0-5 3-9 8-11z"></path>-->
		                    <path fill="#ffeac8" d="M103 373c10 2 14 10 8 19 6-1 10 4 10 9 0 3-3 6-6 7l-26 11c-2 1-5 1-8 0-6-3-7-9-2-16-7-1-13-9-6-17-8-1-12-8-8-15l3-3 23-11c9-4 19 8 12 16z"></path>
		                    <ellipse transform="rotate(173.3 233.455 334.51)" fill="#ce4a1c" cx="234" cy="335" rx="32" ry="46"></ellipse>
		                </svg>
					</div>
					<div class="title mod-complement"><span>Search</span><b>APE</b></div>
				</div>
			</a>
			<div class="mod-complement" style="overflow: initial;">
				<div id="search" class="mod-left">
					<form id="searchInput" action="" method="GET">
						<button class="mod-right"><i class="icon-search"></i></button>
						<div class="input mod-complement"><input type="text" autocomplete="off" name="q" tabindex="1" value="<?=$query?>" autocapitalize="off" autocorrect="off"></div>
						<div class="suggest"></div>
					</form>
				</div>
				<div id="more" class="mod-right"><span>Your quick and secure search.</span> <i class="idown icon-down-open-mini"></i></div>
				<div id="links" class="mod-right">
					<div class="mod">
						<a href="https://www.facebook.com/" target="_blank"><div class="webpage">
							<div class="cart">
								<div class="thumbnail">
									<i class="icon-facebook"></i>
								</div>
								<div class="domain">facebook.com</div>
							</div>
						</div></a>
						<a href="https://twitter.com/" target="_blank"><div class="webpage">
							<div class="cart">
								<div class="thumbnail">
									<i class="icon-twitter"></i>
								</div>
								<div class="domain">twitter.com</div>
							</div>
						</div></a>
						<a href="https://www.instagram.com/" target="_blank"><div class="webpage">
							<div class="cart">
								<div class="thumbnail">
									<i class="icon-instagram"></i>
								</div>
								<div class="domain">instagram.com</div>
							</div>
						</div></a>
						<a href="https://www.pinterest.com/" target="_blank"><div class="webpage">
							<div class="cart">
								<div class="thumbnail">
									<i class="icon-pinterest"></i>
								</div>
								<div class="domain">pinterest.com</div>
							</div>
						</div></a>
						<a href="https://www.youtube.com/" target="_blank"><div class="webpage">
							<div class="cart">
								<div class="thumbnail">
									<i class="icon-youtube-play"></i>
								</div>
								<div class="domain">youtube.com</div>
							</div>
						</div></a>
						<a href="https://www.reddit.com/" target="_blank"><div class="webpage">
							<div class="cart">
								<div class="thumbnail">
									<i class="icon-reddit"></i>
								</div>
								<div class="domain">reddit.com</div>
							</div>
						</div></a>
						<a href="https://www.amazon.com/" target="_blank"><div class="webpage">
							<div class="cart">
								<div class="thumbnail">
									<i class="icon-amazon"></i>
								</div>
								<div class="domain">amazon.com</div>
							</div>
						</div></a>
					</div>
				</div>
			</div>
		</div>
		<nav id="apes">
			<a class="active" href="<?=$dquery."&type=web"?>">Web</a>
			<a href="<?=$dquery."&type=image"?>">Imágenes</a>
			<a href="<?=$dquery."&type=video"?>" class="hide">Videos</a>
			<!--<a href="">Noticias</a>-->
		</nav>
	</header>
	<main>
		<div id="error">
			<img src="https://image.flaticon.com/icons/svg/152/152516.svg">
			<h4>Lo sentimos, no hemos encontrado resultados</h4>
		</div>
		<div id="graphics">
			<div id="result-images"></div>
			<div id="result-videos"></div>
		</div>
		<div id="results" class="mod-left">
			<div id="result-images-example"></div>
			
		</div>
		<div id="widgets" class="mod-complement"></div>
	</main>
	<footer>
		<!--<button id="comentaries">Send Feedback</button>-->
		<nav>
			<a>© SearchApe 2018</a>
			<a href="">About</a>
			<a href="">Privacy</a>
			<a href="">Terms</a>
			<a href="">Cookie Policy</a>
			<a href="">Careers</a>
			<a href="">Help</a>
			<a href="">Feedback</a>
		</nav>
	</footer>
	<script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
	<script type="text/javascript" src="assets/js/lazy.js"></script>
	<script type="text/javascript">
        <?php if(isset($query)){ ?>
		var query = '<?=str_replace("'", "\\'", $query);?>';
		<?php } ?>
        <?php if(isset($_GET["type"])){ ?>
        var type = '<?=$_GET["type"]?>';
		<?php } ?>
        var domain = 'http://<?=$ip?>/';
		var api = "http://<?=$ip?>:81/api/v1.0.0/";
	</script>
	<script type="text/javascript" src="assets/js/script.js"></script>
</body>
</html>